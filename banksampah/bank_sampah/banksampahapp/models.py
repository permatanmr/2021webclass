from django.db import models
from django.db.models.fields.files import ImageField

# Create your models here.
class Titik_Pengambilan(models.Model):
    nama_tempat = models.CharField(max_length=100)
    gambar = ImageField()
    koordinat_latitude = models.CharField(max_length=10)
    koordinat_longitude = models.CharField(max_length=10)

    def __str__(self):
        return "{} ({}, {} )".format(self.nama_tempat, self.koordinat_latitude, self.koordinat_longitude)

class Item(models.Model):
    Harga_perKilo = models.IntegerField()
    Plastik = 'Plastik'
    Logam = 'Logam'
    Kertas = 'Kertas'
    Kardus = 'Kardus'
    Beling = 'Beling'
    Jenis_sampah = [
        (Plastik, 'plastik'),
        (Logam, 'logam'),
        (Kertas, 'kertas'),
        (Kardus, 'kardus'),
        (Beling, 'beling'),
    ]
    Pilihan_sampah = models.CharField(
        max_length=7,
        choices=Jenis_sampah,
        default=Plastik,
    )

    def __str__(self):
        return self.Pilihan_sampah

class Setor_Sampah(models.Model):
    Item = models.ForeignKey(Item, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    Titik_Pengambilan = models.ForeignKey(Titik_Pengambilan, on_delete=models.CASCADE)


    def __str__(self):
        return self.Item, self.quantity


