from django.apps import AppConfig


class BanksampahappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'banksampahapp'
