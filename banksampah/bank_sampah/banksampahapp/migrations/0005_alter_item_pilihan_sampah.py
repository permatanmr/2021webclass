# Generated by Django 3.2.7 on 2021-11-09 02:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('banksampahapp', '0004_auto_20211109_0947'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='Pilihan_sampah',
            field=models.CharField(choices=[('Plastik', 'plastik'), ('Logam', 'logam'), ('Kertas', 'kertas'), ('Kardus', 'kardus'), ('Beling', 'beling')], default='Plastik', max_length=10),
        ),
    ]
