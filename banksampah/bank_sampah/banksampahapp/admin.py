from django.contrib import admin
from django.db.models.base import Model
from . import models

# Register your models here.
admin.site.register(models.Titik_Pengambilan)
admin.site.register(models.Setor_Sampah)
admin.site.register(models.Item)